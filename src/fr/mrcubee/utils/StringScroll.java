package fr.mrcubee.utils;

public class StringScroll {
	
	private char[] chars;
	private int viewLength;
	private int current;
	
	private StringScroll(String string, int viewLength) {
		this.chars = string.toCharArray();
		this.viewLength = viewLength;
		this.current = 0;
	}
	
	@Override
	public String toString() {
		StringBuilder stringBuilder = new StringBuilder();
		
		for (int i = current, v = 0; v < viewLength; i++, v++) {
			if (i == chars.length)
				i = 0;
			stringBuilder.append(chars[i]);
		}
		return stringBuilder.toString();
	}
	
	public void setCurrent(int current) {
		if (current >= chars.length)
			current = current - ((current / chars.length) * chars.length);
		else if (current < 0) {
			current *= -1;
			current = chars.length - (current - (current / chars.length) * chars.length);
		}
		this.current = current;
	}
	
	public int getCurrent() {
		return current;
	}
	
	public int getLengt() {
		return chars.length;
	}
	
	public int getView_length() {
		return viewLength;
	}
	
	public static StringScroll create(String sentence, int viewLength) {
		if (sentence == null || sentence.isEmpty() || viewLength <= 0)
			return null;
		return new StringScroll(sentence, viewLength);
	}
	
}
