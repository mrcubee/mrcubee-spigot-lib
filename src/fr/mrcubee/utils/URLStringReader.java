package fr.mrcubee.utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;

public class URLStringReader {
	
	public static String readURL(URL url) {
		String result = null;
		String line;
		BufferedReader bufferedReader = null;
		
		if (url == null)
			return null;
		try {
			bufferedReader = new BufferedReader(new InputStreamReader(url.openStream()));
			while ((line = bufferedReader.readLine()) != null) {
				if (result == null)
					result = line;
				else
					result = result + "\n" + line;
			}
			bufferedReader.close();
		} catch (IOException e) {
			return null;
		}
		return result;
	}
	
	public static String readURLFromString(String urlString) {
		URL url = null;
		
		if (urlString == null || urlString.isEmpty())
			return null;
		try {
			url = new URL(urlString);
		} catch (MalformedURLException e) {}
		return readURL(url);
	}

}
