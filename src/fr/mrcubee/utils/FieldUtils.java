package fr.mrcubee.utils;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;

import org.bukkit.Bukkit;

public class FieldUtils {
	
	public static <T> void setValue(Class<T> clazz, T object, String fieldName, Object value) throws NoSuchFieldException, SecurityException, IllegalArgumentException, IllegalAccessException {
		Field modifier;
		Field field;
		
		if (clazz == null || object == null || fieldName == null || fieldName.isEmpty())
			return;
		if ((field = clazz.getDeclaredField(fieldName)) == null)
			return;
		if ((modifier = Field.class.getDeclaredField("modifiers")) == null)
			return;
		field.setAccessible(true);
		modifier.setAccessible(true);
		modifier.setInt(field, field.getModifiers() & ~Modifier.FINAL);
		field.set(object, value);
	}
	
	public static <T> Object getValue(Class<T> clazz, T object, String fieldName) throws NoSuchFieldException, SecurityException, IllegalArgumentException, IllegalAccessException {
		Field field;
		
		if (clazz == null || object == null || fieldName == null || fieldName.isEmpty())
			return null;
		if ((field = clazz.getDeclaredField(fieldName)) == null)
			return null;
		field.setAccessible(true);
		return field.get(object);
	}

}
