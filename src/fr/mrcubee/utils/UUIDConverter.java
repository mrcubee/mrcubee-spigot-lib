package fr.mrcubee.utils;

import java.util.UUID;

import fr.mrcubee.plugin.utils.MinecraftValues;

public class UUIDConverter {
	
	public static String toTrimmedUUID(UUID uuid) {
		if (uuid == null)
			return null;
		return uuid.toString().replaceAll("-", "");
	}
	
	public static UUID getUUID(String uuidString) {
		int length;
		StringBuilder stringBuilder;
		
		if (uuidString == null || uuidString.isEmpty())
			return null;
		if ((length = uuidString.length()) == MinecraftValues.UUID_FULL_LENGTH)
			return UUID.fromString(uuidString);
		else if (length == MinecraftValues.UUID_TRIMMED_LENGTH) {
			stringBuilder = new StringBuilder(uuidString.trim());
			try {
				stringBuilder.insert(20, "-");
				stringBuilder.insert(16, "-");
				stringBuilder.insert(12, "-");
				stringBuilder.insert(8, "-");
			} catch (StringIndexOutOfBoundsException e) {
				return null;
			}
			return UUID.fromString(stringBuilder.toString());
		}
		return null;
	}

}
