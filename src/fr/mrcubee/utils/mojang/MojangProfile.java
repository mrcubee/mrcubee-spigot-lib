package fr.mrcubee.utils.mojang;

import java.util.UUID;

import org.bukkit.Bukkit;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import com.mojang.authlib.GameProfile;
import com.mojang.authlib.properties.Property;

import fr.mrcubee.plugin.utils.MinecraftValues;
import fr.mrcubee.utils.URLStringReader;
import fr.mrcubee.utils.UUIDConverter;

public class MojangProfile {
	
	private UUID uuid;
	private String name;
	private Property[] property;
	
	private MojangProfile(UUID uuid, String name, Property[] property) {
		this.uuid = uuid;
		this.name = name;
		this.property = property;
	}
	
	private static JSONObject getJSONFromURLString(String url) {
		String urlContent = URLStringReader.readURLFromString(url);
		JSONParser jsonParser;
		Object result = null;
		
		if (urlContent == null)
			return null;
		jsonParser = new JSONParser();
		try {
			result = jsonParser.parse(urlContent);
		} catch (ParseException e) {}
		if (result == null || !(result instanceof JSONObject))
			return null;
		if (!(result instanceof JSONObject))
			return null;
		return (JSONObject) result;
	}
	
	private static Property getPlayerProperty(JSONObject propertyJson) {
		if (propertyJson == null || !propertyJson.containsKey("name") || !propertyJson.containsKey("value")
				|| !propertyJson.containsKey("signature"))
			return null;
		return new Property(propertyJson.get("name").toString(), propertyJson.get("value").toString(),
				propertyJson.get("signature").toString());
	}
	
	private static Property[] getPlayerPropertys(JSONObject jsonObject) {
		Object propertyArrayObject;
		JSONArray propertyJsonArray;
		Property[] result;
		int i = 0;
		
		if (jsonObject == null || !jsonObject.containsKey("properties")
				|| !((propertyArrayObject = jsonObject.get("properties")) instanceof JSONArray))
			return null;
		propertyJsonArray = (JSONArray) propertyArrayObject;
		if (propertyJsonArray.isEmpty())
			return null;
		result = new Property[propertyJsonArray.size()];
		for (Object object : propertyJsonArray.toArray()) {
			if (object instanceof JSONObject)
				result[i] = getPlayerProperty((JSONObject) object);
			i++;
		}
		return result;
	}
	
	public static MojangProfile getMojangProfile(String nameOrUUID) {
		int lenght;
		
		if (nameOrUUID.isEmpty())
			return null;
		if ((lenght = nameOrUUID.length()) == MinecraftValues.UUID_FULL_LENGTH || lenght == MinecraftValues.UUID_TRIMMED_LENGTH)
			return getMojangProfileFromUUID(UUIDConverter.getUUID(nameOrUUID));
		else if (lenght <= MinecraftValues.PLAYER_NAME_LENGTH_MAX)
			return getMojangProfileFromName(nameOrUUID);
		return null;
	}
	
	public static MojangProfile getMojangProfileFromUUID(UUID uuid) {
		JSONObject json;
		String name;
		Property[] property;
		
		if (uuid == null)
			return null;
		json = getJSONFromURLString("https://sessionserver.mojang.com/session/minecraft/profile/" + UUIDConverter.toTrimmedUUID(uuid) + "?unsigned=false");

		if (json == null || !json.containsKey("id") || !json.containsKey("name") || (property = getPlayerPropertys(json)) == null)
			return null;
		name = json.get("name").toString();
		return new MojangProfile(uuid, name, property);
	}
	
	public static MojangProfile getMojangProfileFromName(String name) {
		JSONObject json;
		
		if (name == null || name.isEmpty())
			return null;
		json = getJSONFromURLString("https://api.mojang.com/users/profiles/minecraft/" + name);
		if (json == null || !json.containsKey("id"))
			return null;
		return getMojangProfileFromUUID(UUIDConverter.getUUID(json.get("id").toString()));
	}
	
	public UUID getUUID() {
		return uuid;
	}
	
	public String getName() {
		return name;
	}
	
	public Property[] getProperty() {
		return property;
	}
	
	public GameProfile generateGameProgile() {
		GameProfile gameProfile = new GameProfile(uuid, name);
		
		for (Property property : this.property)
			if (property != null)
				gameProfile.getProperties().put(property.getName(), property);
		return gameProfile;
	}
}
