package fr.mrcubee.plugin.utils;

public class MinecraftValues {
	
	public static final int PLAYER_NAME_LENGTH_MIN = 3;
	public static final int PLAYER_NAME_LENGTH_MAX = 16;
	public static final int UUID_FULL_LENGTH = 36;
	public static final int UUID_TRIMMED_LENGTH = 32;
	public static final int INVENTORY_NAME_LENGTH_MAX = 32;
	public static final int BOSS_MAR_LENGTH_MAX = 64;

}
