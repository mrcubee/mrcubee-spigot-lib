package fr.mrcubee.plugin.spigot.utils.annotation.manager;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.Map;
import java.util.Map.Entry;

public class AnnotationLoader {
	
	public static <T extends Annotation> void loadAnnotationFields(Class<T> annotationClass, Object object, FieldLoader<T> fieldLoader) {
		Map<Field, T> result = AnnotationExtractor.getDeclaredFields(annotationClass, object);
		
		if (fieldLoader == null)
			return;
		result = AnnotationExtractor.getDeclaredFields(annotationClass, object);
		if (result == null)
			return;
		for (Entry<Field, T> entry : result.entrySet())
			fieldLoader.load(entry.getKey(), entry.getValue(), object);
	}
	
	public static <T extends Annotation> void loadAnnotationMethods(Class<T> annotationClass, Object object, MethodLoader<T> methodLoader) {
		Map<Method, T> result;
		
		if (methodLoader == null)
			return;
		result = AnnotationExtractor.getDeclaredMethods(annotationClass, object);
		if (result == null)
			return;
		for (Entry<Method, T> entry : result.entrySet())
			methodLoader.load(entry.getKey(), entry.getValue(), object);
	}

}
