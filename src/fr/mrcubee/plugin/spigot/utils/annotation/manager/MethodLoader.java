package fr.mrcubee.plugin.spigot.utils.annotation.manager;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;

public interface MethodLoader<T extends Annotation> {
	
	public void load(Method method, T annotation, Object object);

}
