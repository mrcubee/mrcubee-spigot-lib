package fr.mrcubee.plugin.spigot.utils.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface Permission {
	String permission();
	String errorMessage() default "You do not have permission.";
	boolean color() default false;
	char colorChar() default '&';
}
