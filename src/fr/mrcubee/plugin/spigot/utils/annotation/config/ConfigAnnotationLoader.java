package fr.mrcubee.plugin.spigot.utils.annotation.config;

import java.lang.reflect.Field;
import java.util.List;

import org.bukkit.ChatColor;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.plugin.Plugin;

import fr.mrcubee.plugin.spigot.utils.annotation.Config;
import fr.mrcubee.plugin.spigot.utils.annotation.manager.AnnotationLoader;
import fr.mrcubee.plugin.spigot.utils.annotation.manager.FieldLoader;
import fr.mrcubee.plugin.spigot.utils.annotation.manager.PluginAnnotationsLoader;

public class ConfigAnnotationLoader implements PluginAnnotationsLoader {

	public void loadClass(Plugin plugin, Object... objects) {
		if (plugin == null || objects == null)
			return;
		for (Object object : objects)
			loadObject(plugin, object);
	}
	
	private Object getValue(FileConfiguration fileConfiguration, Config annotation) {
		Object value;
		String[] tabValues;
		List<Object> listValues;
		Object elementValue;
		
		if (fileConfiguration == null || annotation.path() == null || annotation.path().isEmpty()
				|| !fileConfiguration.contains(annotation.path()))
			return null;
		value = fileConfiguration.get(annotation.path());
		
		if (value == null || !annotation.color())
			return value;
		if (value instanceof String)
			return ChatColor.translateAlternateColorCodes(annotation.colorChar(), (String) value);
		if (value instanceof String[]) {
			tabValues = (String[]) value;
			for (int i = 0; i < tabValues.length; i++)
				tabValues[i] = ChatColor.translateAlternateColorCodes(annotation.colorChar(), tabValues[i]);
			return tabValues;
		}
		if (value instanceof List) {
			listValues = (List<Object>) value;
			
			for (int i = 0; i < listValues.size(); i++)
				if ((elementValue = listValues.get(i)) instanceof String)
					listValues.set(i, ChatColor.translateAlternateColorCodes(annotation.colorChar(), (String) elementValue));
			return listValues;
		}
		return value;
	}
	
	private void loadObject(Plugin plugin, Object object) {
		FileConfiguration config = plugin.getConfig();
		
		AnnotationLoader.loadAnnotationFields(Config.class, object, new FieldLoader<Config>() {
			@Override
			public void load(Field field, Config annotation, Object object) {
				Object value = getValue(config, annotation);
				
				if (value == null)
					return;
				field.setAccessible(true);
				try {
					field.set(object, value);
				} catch (IllegalArgumentException | IllegalAccessException e) {}
			}
		});
	}
}
