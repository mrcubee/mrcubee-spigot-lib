package fr.mrcubee.plugin.spigot.utils.annotation.command;

import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.command.PluginCommand;
import org.bukkit.command.TabCompleter;
import org.bukkit.plugin.Plugin;

import fr.mrcubee.plugin.spigot.utils.annotation.Command;
import fr.mrcubee.plugin.spigot.utils.annotation.Permission;
import fr.mrcubee.plugin.spigot.utils.annotation.manager.AnnotationExtractor;
import fr.mrcubee.plugin.spigot.utils.annotation.manager.AnnotationLoader;
import fr.mrcubee.plugin.spigot.utils.annotation.manager.MethodLoader;
import fr.mrcubee.plugin.spigot.utils.annotation.manager.ObjectMethod;
import fr.mrcubee.plugin.spigot.utils.annotation.manager.PluginAnnotationsLoader;

public class CommandAnnotationLoader implements PluginAnnotationsLoader {

	private Map<String, ObjectMethod> getCommandCompletion(Object object) {
		Map<String, ObjectMethod> result;
		
		if (object == null)
			return null;
		result = new HashMap<String, ObjectMethod>();
		AnnotationLoader.loadAnnotationMethods(CommandTabComplete.class, object, new MethodLoader<CommandTabComplete>() {
			@Override
			public void load(Method method, CommandTabComplete annotation, Object object) {
				PluginCommand pluginCommand;
				TabCompleter tabCompleter;
				
				if (annotation.command() == null || annotation.command().isEmpty())
					return;
				if ((pluginCommand = Bukkit.getPluginCommand(annotation.command())) != null)
					if ((tabCompleter = pluginCommand.getTabCompleter()) != null && (tabCompleter instanceof AnnotedCommand))
						return;
				result.put(annotation.command(), new ObjectMethod(object, method));
			}
		});
		return result;
	}
	
	
	@Override
	public void loadClass(Plugin plugin, Object... objects) {
		Map<String, ObjectMethod> tabCompletionMethods = null;
		Map<String, ObjectMethod> resultCompletionMethod;
		Map<Method, Permission> permissions = null;
		Map<Method, Permission> resultPermission;
		
		if (plugin == null || objects == null)
			return;
		for (int i = 0; i < objects.length; i++) {
			resultCompletionMethod = getCommandCompletion(objects[i]);
			resultPermission = AnnotationExtractor.getDeclaredMethods(Permission.class, objects[i]);
			if (resultCompletionMethod != null) {
				if (tabCompletionMethods == null)
					tabCompletionMethods = resultCompletionMethod;
				else
					tabCompletionMethods.putAll(resultCompletionMethod);
			}
			if (resultPermission != null) {
				if (permissions == null)
					permissions = resultPermission;
				else
					permissions.putAll(resultPermission);
			}
		}
		for (Object object : objects)
			registerCommand(plugin, object, permissions, tabCompletionMethods);
	}
	
	private boolean checkMethodParemeters(Method method, Class<?> returnValueType, Class<?>... parameters) {
		Class<?>[] methodParameters = method.getParameterTypes();
		
		if (methodParameters.length != parameters.length)
			return false;
		else if ((returnValueType == null && method.getReturnType() != null) || !returnValueType.equals(method.getReturnType()))
			return false;
		for (int i = 0; i < methodParameters.length; i++)
			if (parameters[i] != null && !methodParameters[i].equals(parameters[i]))
				return false;
		return true;
	}
	
	private void registerCommand(Plugin plugin, Object object, Map<Method, Permission> permissions, Map<String, ObjectMethod> tabCompletionMethods) {
		AnnotationLoader.loadAnnotationMethods(Command.class, object, new MethodLoader<Command>() {
			@Override
			public void load(Method method, Command annotation, Object object) {
				AnnotedCommand annotedCommand;
				PluginCommand pluginCommand = Bukkit.getPluginCommand(annotation.command());
				Permission permission;
				ObjectMethod tabCompletionMethod;
				
				if (!checkMethodParemeters(method, boolean.class, CommandSender.class, String[].class))
					return;
				if (pluginCommand != null && pluginCommand.getPlugin().equals(plugin)
						&& (pluginCommand.getExecutor() instanceof AnnotedCommand))
					return;
				permission = (permissions != null) ? permissions.get(method) : null;
				tabCompletionMethod = (tabCompletionMethods != null) ? tabCompletionMethods.get(annotation.command()) : null;
				if (tabCompletionMethod != null && !checkMethodParemeters(tabCompletionMethod.getMethod(), List.class, CommandSender.class, String[].class))
					tabCompletionMethod = null;
				annotedCommand = new AnnotedCommand(annotation, permission, new ObjectMethod(object, method), tabCompletionMethod);
				pluginCommand.setExecutor(annotedCommand);
				pluginCommand.setTabCompleter(annotedCommand);
			}
		});
	}
}
