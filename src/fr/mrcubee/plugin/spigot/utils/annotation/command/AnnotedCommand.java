package fr.mrcubee.plugin.spigot.utils.annotation.command;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;

import org.bukkit.ChatColor;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabCompleter;

import fr.mrcubee.plugin.spigot.utils.annotation.Command;
import fr.mrcubee.plugin.spigot.utils.annotation.Permission;
import fr.mrcubee.plugin.spigot.utils.annotation.manager.ObjectMethod;

public class AnnotedCommand implements CommandExecutor, TabCompleter {

	private Command commandAnnotation;
	private Permission permissionAnnotation;
	private ObjectMethod commandMethod;
	private ObjectMethod tabCompletMethod;

	protected AnnotedCommand(Command command, Permission permission, ObjectMethod commandMethod,
			ObjectMethod tabCompletMethod) {
		this.commandAnnotation = command;
		this.permissionAnnotation = permission;
		this.commandMethod = commandMethod;
		this.tabCompletMethod = tabCompletMethod;
	}

	@Override
	public boolean onCommand(CommandSender sender, org.bukkit.command.Command command, String label, String[] args) {
		if (this.permissionAnnotation != null && !sender.hasPermission(this.permissionAnnotation.permission())) {
			if (permissionAnnotation.color())
				sender.sendMessage(ChatColor.translateAlternateColorCodes(this.permissionAnnotation.colorChar(),
						this.permissionAnnotation.errorMessage()));
			else
				sender.sendMessage(this.permissionAnnotation.errorMessage());
			return false;
		} else if (args.length < this.commandAnnotation.minArguments()) {
			if (this.commandAnnotation.color())
				sender.sendMessage(ChatColor.translateAlternateColorCodes(this.commandAnnotation.colorChar(),
						this.commandAnnotation.usage()));
			else
				sender.sendMessage(this.commandAnnotation.usage());
			return false;
		}
		try {
			return (boolean) commandMethod.getMethod().invoke(commandMethod.getObject(), sender, args);
		} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {}
		return false;
	}

	@Override
	public List<String> onTabComplete(CommandSender sender, org.bukkit.command.Command command, String label,
			String[] args) {
		List<?> values = null;
		List<String> result;

		if (tabCompletMethod == null)
			return null;
		try {
			values = (List<?>) tabCompletMethod.getMethod().invoke(tabCompletMethod.getObject(), sender, args);
		} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
		}
		if (values == null)
			return null;
		result = new ArrayList<String>();
		for (Object o : values)
			if (o instanceof String)
				result.add((String) o);
		return result;
	}

}
