package fr.mrcubee.plugin.spigot.utils.annotation.command;

import org.bukkit.command.BlockCommandSender;
import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;

public enum CommandSenderType {
	ALL(null),
	CONSOL(ConsoleCommandSender.class),
	PLAYER(Player.class),
	COMMAND_BLOCK(BlockCommandSender.class),
	ENTITY(Entity.class);
	
	private Class<? extends CommandSender> commandSenderClass;
	
	private CommandSenderType(Class<? extends CommandSender> commandSenderClass) {
		this.commandSenderClass = commandSenderClass;
	}

	public Class<? extends CommandSender> getCommandSenderClass() {
		return commandSenderClass;
	}
}
