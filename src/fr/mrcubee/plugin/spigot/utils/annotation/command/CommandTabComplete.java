package fr.mrcubee.plugin.spigot.utils.annotation.command;

public @interface CommandTabComplete {
	String command();
}
