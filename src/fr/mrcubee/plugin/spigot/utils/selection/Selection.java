package fr.mrcubee.plugin.spigot.utils.selection;

import java.util.function.Consumer;

import org.bukkit.Chunk;
import org.bukkit.Location;
import org.bukkit.block.Block;

import fr.mrcubee.plugin.spigot.GameAPI;
import fr.mrcubee.plugin.spigot.utils.tasks.Tasks;
import fr.mrcubee.plugin.spigot.utils.tasks.listener.TaskUpdate;

public class Selection {
	
	private Location min;
	private Location max;
	
	
	private Selection(Location loc1, Location loc2) {
		double minX = (loc1.getX() <= loc2.getX()) ? loc1.getX() : loc2.getX();
		double minY = (loc1.getY() <= loc2.getY()) ? loc1.getY() : loc2.getY();
		double minZ = (loc1.getZ() <= loc2.getZ()) ? loc1.getZ() : loc2.getZ();
		double maxX = (loc1.getX() > loc2.getX()) ? loc1.getX() : loc2.getX();
		double maxY = (loc1.getY() > loc2.getY()) ? loc1.getY() : loc2.getY();
		double maxZ = (loc1.getZ() > loc2.getZ()) ? loc1.getZ() : loc2.getZ();
		
		min = new Location(loc1.getWorld(), minX, minY, minZ);
		max = new Location(loc2.getWorld(), maxX, maxY, maxZ);
	}
	
	public void updateAllBlock(double second, Consumer<Block> consumer) {
		if (consumer == null || second <= 0)
			return;
		
		final Location loc = min.clone();
		Tasks.createRepeatTask(GameAPI.getInstance(), new TaskUpdate() {
			
			@Override
			public boolean update() {
				consumer.accept(loc.getBlock());
				loc.setX(loc.getBlockX() + 1);
				if (loc.getBlockY() >= getMaxBlockY())
					return false;
				if (loc.getBlockZ() >= getMaxBlockZ()) {
					loc.setY(loc.getBlockY() + 1);
					loc.setZ(getMinZ());
				}
				if (loc.getBlockX() >= getMaxBlockX()) {
					loc.setZ(loc.getBlockZ() + 1);
					loc.setX(getMinX());
				}
				return true;
			}
		}, 0, second);
	}
	
	public void updateAllChunk(double second, Consumer<Chunk> consumer) {
		if (consumer == null || second <= 0)
			return;
		
		final Location loc = min.clone();
		Tasks.createRepeatTask(GameAPI.getInstance(), new TaskUpdate() {
			
			@Override
			public boolean update() {
				consumer.accept(loc.getChunk());
				loc.setX(loc.getBlockX() + 16);
				if (loc.getBlockZ() >= getMaxBlockZ())
					return false;
				if (loc.getBlockX() >= getMaxBlockX()) {
					loc.setZ(loc.getBlockZ() + 16);
					loc.setX(getMinX());
				}
				return true;
			}
		}, 0, second);
	}
	
	public double getMinX() {
		return min.getX();
	}
	
	public double getMinY() {
		return min.getY();
	}
	
	public double getMinZ() {
		return min.getZ();
	}
	
	public int getMinBlockX() {
		return min.getBlockX();
	}
	
	public int getMinBlockY() {
		return min.getBlockY();
	}
	
	public int getMinBlockZ() {
		return min.getBlockZ();
	}
	
	public double getMaxX() {
		return max.getX();
	}
	
	public double getMaxY() {
		return max.getY();
	}
	
	public double getMaxZ() {
		return max.getZ();
	}
	
	public int getMaxBlockX() {
		return max.getBlockX();
	}
	
	public int getMaxBlockY() {
		return max.getBlockY();
	}
	
	public int getMaxBlockZ() {
		return max.getBlockZ();
	}
	
	public double getWidth() {
		return max.getX() - min.getX() + 1;
	}
	
	public double getHight() {
		return max.getY() - min.getY() + 1;
	}
	
	public double getLength() {
		return max.getZ() - min.getZ() + 1;
	}
	
	public double getSize() {
		return getWidth() * getHight() * getLength();
	}
	
	public Location getMax() {
		return max.clone();
	}
	
	public Location getMin() {
		return min.clone();
	}
	
	public static Selection createSelection(Location loc1, Location loc2) {
		if (loc1 == null || loc1.getWorld() == null || loc2 == null || loc2.getWorld() == null)
			return null;
		else if (loc1.getWorld() != loc2.getWorld())
			return null;
		return new Selection(loc1, loc2);
	}

}
