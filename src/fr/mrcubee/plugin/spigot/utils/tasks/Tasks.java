package fr.mrcubee.plugin.spigot.utils.tasks;

import org.bukkit.plugin.Plugin;
import org.bukkit.scheduler.BukkitTask;

import fr.mrcubee.plugin.spigot.utils.tasks.listener.TaskUpdate;
import fr.mrcubee.plugin.spigot.utils.tasks.listener.TimerUpdate;

public class Tasks {							
	
	public static BukkitTask createTimer(Plugin plugin, TimerUpdate listener, int from, int to, double startAfterSecond, double updateSecond) {
		if (plugin == null || listener == null || from > to || updateSecond <= 0)
			return null;
		return new TimerRunnable(listener, from, to).runTaskTimer(plugin, (long) (20L * startAfterSecond), (long) (20L * updateSecond));
	}
	
	public static BukkitTask createRepeatTask(Plugin plugin, TaskUpdate listener, double startAfterSecond, double updateSecond) {
		if (plugin == null || listener == null)
			return null;
		return new RepeatTaskRunnable(listener).runTaskTimer(plugin, (long) (20L * startAfterSecond), (long) (20L * updateSecond));
	}
	
}
