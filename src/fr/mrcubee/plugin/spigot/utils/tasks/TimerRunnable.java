package fr.mrcubee.plugin.spigot.utils.tasks;

import org.bukkit.scheduler.BukkitRunnable;

import fr.mrcubee.plugin.spigot.utils.tasks.listener.TimerUpdate;

public class TimerRunnable extends BukkitRunnable {
	
	private TimerUpdate timerUpdate;
	
	private boolean cancel;
	private int min;
	private int max;
	private int current;
	
	protected TimerRunnable(TimerUpdate timerUpdate, int min, int max) {
		this.cancel = false;
		this.timerUpdate = timerUpdate;
		this.current = (this.min = min);
		this.max = max;
	}

	@Override
	public void run() {
		if (cancel || current > max) {
			this.cancel();
			return;
		}
		if (!timerUpdate.update(current, min, max))
			this.cancel();
		this.current++;
	}
	
	@Override
	public synchronized void cancel() throws IllegalStateException {
		this.cancel = true;
		super.cancel();
	}

}
