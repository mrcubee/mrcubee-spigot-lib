package fr.mrcubee.plugin.spigot.utils.tasks;

import org.bukkit.scheduler.BukkitRunnable;

import fr.mrcubee.plugin.spigot.utils.tasks.listener.TaskUpdate;

public class RepeatTaskRunnable extends BukkitRunnable {
	
	private TaskUpdate taskUpdate;
	
	private boolean cancel;
	
	protected RepeatTaskRunnable(TaskUpdate taskUpdate) {
		this.cancel = false;
		this.taskUpdate = taskUpdate;
	}

	@Override
	public void run() {
		if (cancel) {
			this.cancel();
			return;
		}
		if (!taskUpdate.update())
			this.cancel();
	}
	
	@Override
	public synchronized void cancel() throws IllegalStateException {
		this.cancel = true;
		super.cancel();
	}

}
