package fr.mrcubee.plugin.spigot.utils.tasks.listener;

public interface TaskUpdate {
	
	public boolean update();

}
