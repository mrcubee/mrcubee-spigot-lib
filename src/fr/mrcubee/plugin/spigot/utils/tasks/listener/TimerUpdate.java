package fr.mrcubee.plugin.spigot.utils.tasks.listener;

public interface TimerUpdate {
	
	public boolean update(int current, int min, int max);

}
