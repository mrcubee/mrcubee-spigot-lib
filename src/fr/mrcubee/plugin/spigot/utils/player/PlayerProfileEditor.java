package fr.mrcubee.plugin.spigot.utils.player;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.UUID;

import org.bukkit.entity.Player;

import com.mojang.authlib.properties.Property;

import fr.mrcubee.plugin.spigot.utils.ServerVersion;
import fr.mrcubee.utils.mojang.MojangProfile;

public class PlayerProfileEditor {
	
	private static Method getMethod(String methodName, Class<?>... parameters) {
		Method method = null;
		
		try {
			Class<?> clazz = Class.forName(
					"fr.mrcubee.plugin.spigot." + ServerVersion.getPackageVersion() + ".utils.player.PlayerProfileEditor");
			method = clazz.getMethod(methodName, parameters);
		} catch (Exception e) {}
		return method;
	}

	public static void setSkin(Player player, Property[] properties) {
		Method method;
		
		if ((method = getMethod("setSkin", Player.class, Property[].class)) == null)
			return;
		try {
			method.invoke(null, player, properties);
		} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {}
	}

	public static void setName(Player player, String name) {
		Method method;
		
		if ((method = getMethod("setName", Player.class, String.class)) == null)
			return;
		try {
			method.invoke(null, player, name);
		} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {}
	}

	public static void setUUID(Player player, UUID uuid) {
		Method method;
		
		if ((method = getMethod("setUUID", Player.class, UUID.class)) == null)
			return;
		try {
			method.invoke(null, player, uuid);
		} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {}
	}
	
	public static void setProfile(Player player, MojangProfile mojangProfile, boolean isSetId) {
		Method method;
		
		if ((method = getMethod("setProfile", Player.class, MojangProfile.class, boolean.class)) == null)
			return;
		try {
			method.invoke(null, player, mojangProfile, isSetId);
		} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {}
	}
	
	public static void updatePlayer(Player player) {
		Method method;
		
		if ((method = getMethod("updatePlayer", Player.class)) == null)
			return;
		try {
			method.invoke(null, player);
		} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {}
	}
}
