package fr.mrcubee.plugin.spigot.utils.itemstack;

import java.util.Arrays;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class ItemStackBuilder {
	
	private static String[] setColors(String[] lores) {
		if (lores == null)
			return null;
		for (int i = 0; i < lores.length; i++)
			lores[i] = ChatColor.translateAlternateColorCodes('&', lores[i]);
		return lores;
	}
	
	public static ItemStack create(String displayName, Material material, int amount, String... lores) {
		ItemStack itemStack = new ItemStack(material, amount);
		ItemMeta itemMeta = itemStack.getItemMeta();
		
		if (displayName != null && !displayName.isEmpty())
			itemMeta.setDisplayName(ChatColor.translateAlternateColorCodes('&', displayName));
		if (lores != null)
			itemMeta.setLore(Arrays.asList(setColors(lores)));
		itemStack.setItemMeta(itemMeta);
		return itemStack;
	}
	
	public static ItemStack create(String displayName, Material material, String... lores) {
		return create(displayName, material, 1, lores);
	}

}
