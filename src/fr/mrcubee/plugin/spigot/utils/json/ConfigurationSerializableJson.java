package fr.mrcubee.plugin.spigot.utils.json;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import org.bukkit.Location;
import org.bukkit.configuration.serialization.ConfigurationSerializable;
import org.bukkit.configuration.serialization.ConfigurationSerialization;
import org.bukkit.inventory.ItemStack;
import org.json.simple.JSONObject;

import com.google.gson.JsonObject;

public class ConfigurationSerializableJson {
	
	private static JSONObject mapToJson(Map<String, Object> map) {
		JSONObject jsonObject;
		
		if (map == null || map.isEmpty())
			return null;
		jsonObject = new JSONObject();
		for (Entry<String, Object> entry : map.entrySet())
			jsonObject.put(entry.getKey(), entry.getValue());
		return jsonObject;
	}
	
	private static Map<String, Object> jsonToMap(Object object) {
		JSONObject jsonObject;
		Map<String, Object> map;
		
		if (object == null || !(object instanceof JsonObject) || (jsonObject = (JSONObject) object).isEmpty())
			return null;
		map = new HashMap<String, Object>();
		for (Object key : jsonObject.keySet())
			if (key instanceof String)
				map.put((String) key, jsonObject.get((String) key));
		return map;
	}
	
	public static <T extends ConfigurationSerializable> JSONObject configurationSerializableToJson(Class<T> clazz, T object) {
		Map<String, Object> maps;
		
		if (object == null)
			return null;
		maps = object.serialize();
		if (maps != null)
			maps.put("==", clazz.getName());
		return mapToJson(object.serialize());
	}
	
	public static ConfigurationSerializable jsonToConfigurationSerializable(Object object) {
		Map<String, Object> map = jsonToMap(object);
		
		if (map == null)
			return null;
		return ConfigurationSerialization.deserializeObject(map);
	}
	
	public static ItemStack jsonToItemStack(Object object) {
		ConfigurationSerializable serializable = jsonToConfigurationSerializable(object);
		
		if (serializable == null || !(serializable instanceof ItemStack))
			return null;
		return (ItemStack) serializable;
	}
	
	public static Location jsonToLocation(Object object) {
		ConfigurationSerializable serializable = jsonToConfigurationSerializable(object);
		
		if (serializable == null || !(serializable instanceof Location))
			return null;
		return (Location) serializable;
	}
}
