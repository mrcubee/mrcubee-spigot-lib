package fr.mrcubee.plugin.spigot.v1_8_R3.utils.player;

import java.util.Map;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.craftbukkit.v1_8_R3.CraftServer;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftPlayer;
import org.bukkit.entity.Player;

import com.mojang.authlib.GameProfile;
import com.mojang.authlib.properties.Property;
import com.mojang.authlib.properties.PropertyMap;

import fr.mrcubee.plugin.utils.MinecraftValues;
import fr.mrcubee.utils.FieldUtils;
import fr.mrcubee.utils.mojang.MojangProfile;
import net.minecraft.server.v1_8_R3.EntityPlayer;
import net.minecraft.server.v1_8_R3.Packet;
import net.minecraft.server.v1_8_R3.PacketPlayOutEntityDestroy;
import net.minecraft.server.v1_8_R3.PacketPlayOutNamedEntitySpawn;
import net.minecraft.server.v1_8_R3.PacketPlayOutPlayerInfo;
import net.minecraft.server.v1_8_R3.PacketPlayOutPlayerInfo.EnumPlayerInfoAction;
import net.minecraft.server.v1_8_R3.PacketPlayOutRespawn;
import net.minecraft.server.v1_8_R3.PlayerConnection;
import net.minecraft.server.v1_8_R3.PlayerList;
import net.minecraft.server.v1_8_R3.WorldServer;

public class PlayerProfileEditor {
	
	private static Packet[] getUpdateTabPlayerPackets(EntityPlayer entityPlayer) {
		Packet<?>[] packets = new Packet<?>[2];
		
		packets[0] = new PacketPlayOutPlayerInfo(EnumPlayerInfoAction.REMOVE_PLAYER, entityPlayer);
		packets[1] = new PacketPlayOutPlayerInfo(EnumPlayerInfoAction.ADD_PLAYER, entityPlayer);
		return packets;
	}
	
	private static Packet[] getRespawnPlayerPackets(EntityPlayer entityPlayer, boolean self) {
		WorldServer worldServer = entityPlayer.getWorld().getWorld().getHandle();
		Packet<?>[] packets = new Packet<?>[(self) ? 1 : 2];
		
		if (self)
			packets[0] = new PacketPlayOutRespawn(worldServer.dimension, worldServer.getDifficulty(),
					worldServer.worldData.getType(), entityPlayer.playerInteractManager.getGameMode());
		else {
			packets[0] = new PacketPlayOutEntityDestroy(entityPlayer.getId());
			packets[1] = new PacketPlayOutNamedEntitySpawn(entityPlayer);
		}
		return packets;
	}
	
	private static void sendPacket(Player player, Packet<?>... packets) {
		PlayerConnection playerConnection = ((CraftPlayer) player).getHandle().playerConnection;
		
		for (Packet<?> packet : packets)
			playerConnection.sendPacket(packet);
	}

	public static void updatePlayer(Player player) {
		EntityPlayer entityPlayer;
		Packet<?>[] tabUpdate;
		Packet<?>[] selfUpdate;
		Packet<?>[] playerUpdate;
		
		if (player == null)
			return;
		entityPlayer = ((CraftPlayer) player).getHandle();
		tabUpdate = getUpdateTabPlayerPackets(entityPlayer);
		selfUpdate = getRespawnPlayerPackets(entityPlayer, true);
		playerUpdate = getRespawnPlayerPackets(entityPlayer, false);
		Bukkit.getOnlinePlayers().forEach(p -> sendPacket(p, tabUpdate));
		player.getWorld().getPlayers().forEach(p -> sendPacket(p, (p.equals(player)) ? selfUpdate : playerUpdate));
	}

	public static void setSkin(Player player, Property[] properties) {
		PropertyMap propertyMap;

		if (player == null || properties == null)
			return;
		propertyMap = ((CraftPlayer) player).getHandle().getProfile().getProperties();
		propertyMap.clear();
		for (Property property : properties)
			if (property != null)
				propertyMap.put(property.getName(), property);
	}

	public static void setName(Player player, String name) {
		int length;
		EntityPlayer entityplayer;
		PlayerList playerlist;
		Map<String, EntityPlayer> players;

		if (player == null || name == null || name.isEmpty()
				|| (length = name.length()) < MinecraftValues.PLAYER_NAME_LENGTH_MIN
				|| length > MinecraftValues.PLAYER_NAME_LENGTH_MAX)
			return;
		playerlist = ((CraftServer) player.getServer()).getServer().getPlayerList();
		entityplayer = ((CraftPlayer) player).getHandle();
		try {
			players = (Map<String, EntityPlayer>) FieldUtils.getValue(PlayerList.class, playerlist, "playersByName");
			if (players != null) {
				players.remove(entityplayer.getProfile().getName());
				FieldUtils.setValue(GameProfile.class, entityplayer.getProfile(), "name", name);
				players.put(name, entityplayer);
			} else
				FieldUtils.setValue(GameProfile.class, entityplayer.getProfile(), "name", name);
		} catch (Exception e) {}
		player.setDisplayName(null);
		player.setCustomName(null);
	}

	public static void setUUID(Player player, UUID uuid) {
		EntityPlayer entityplayer;
		PlayerList playerlist;
		Map<UUID, EntityPlayer> players;

		if (player == null || uuid == null)
			return;
		playerlist = ((CraftServer) player.getServer()).getServer().getPlayerList();
		entityplayer = ((CraftPlayer) player).getHandle();
		try {
			players = (Map<UUID, EntityPlayer>) FieldUtils.getValue(PlayerList.class, playerlist, "j");
			if (players != null) {
				players.remove(entityplayer.getProfile().getId());
				FieldUtils.setValue(GameProfile.class, entityplayer.getProfile(), "id", uuid);
				players.put(uuid, entityplayer);
			} else
				FieldUtils.setValue(GameProfile.class, entityplayer.getProfile(), "id", uuid);
		} catch (NoSuchFieldException | SecurityException | IllegalArgumentException | IllegalAccessException e) {}
	}

	public static void setProfile(Player player, MojangProfile mojangProfile, boolean isSetId) {
		setSkin(player, mojangProfile.getProperty());
		setName(player, mojangProfile.getName());
		if (isSetId)
			setUUID(player, mojangProfile.getUUID());
		updatePlayer(player);
	}

}
