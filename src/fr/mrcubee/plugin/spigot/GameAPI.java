package fr.mrcubee.plugin.spigot;

import org.bukkit.plugin.java.JavaPlugin;

/**
 * 
 * @author MrCubee
 *
 */
public class GameAPI extends JavaPlugin {
	
	private static GameAPI instance;
	
	@Override
	public void onLoad() {
		instance = this;
	}
	
	@Override
	public void onEnable() {
		
	}
	
	@Override
	public void onDisable() {
		
	}
	
	public static GameAPI getInstance() {
		return instance;
	}
	
}
